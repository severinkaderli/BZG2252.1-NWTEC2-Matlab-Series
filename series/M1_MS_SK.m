% Serie M1, MS (Marius Schär), SK (Severin Kaderli)
disp("Aufgabe 9");
aa = zeros(1,6);
aa(6) = 4.8;
disp(aa);

disp("Aufgabe 11");
AA = [
    linspace(130,10,7);
    linspace(1,12,7);
    linspace(12,72,7)
];
disp(AA);

disp("Aufgabe 12");
BB = [
    linspace(5,5,4);
    linspace(2,2,4);
    linspace(3,3,4)
];
BB = BB';
disp(BB);

disp("Aufgabe 15");
DD = ones(2,3);
EE = zeros(2,3);
AA = [
    DD EE;
    EE DD
];
AA = AA';
disp(AA);
