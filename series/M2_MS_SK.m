% Serie M2, MS (Marius Schär), SK (Severin Kaderli)
disp("Aufgabe 1");
x = -3:3;
y = x.^2 - exp(0.5*x) + x;
disp(y);
clear x y;

disp("Aufgabe 2");
x = [1.5,2.5,3.5,4.5,5.5,6.6];
y2 = ((x+3).^4) ./ ((x+1) .* sqrt(x));
disp(y2);
clear x y;

disp("Aufgabe 3");

x = -4:0.01:4;
y = x.^2 - exp(0.5*x) + x;
y2 = ((x+3).^4) ./ ((x+1) .* sqrt(x));
figure(4);
plot(x, y);
hold on;
plot(x, y2);
title("Aufgabe 3");

disp("Aufgabe 7 (a)");
x = -4:0.01:3;
f = -3.*x.^4 + 10.*x.^2 - 3;
figure(1);
plot(x, f);
title("Aufgabe 7 (a) - Figure 1");

clear f x;
x = -2:0.01:4;
f = -3.*x.^4 + 10.*x.^2 - 3;
figure(2);
plot(x, f);
title("Aufgabe 7 (a) - Figure 2");

disp("Aufgabe 7 (b)");

x = -4:0.01:3;
f = -3.*x.^4 + 10.*x.^2 - 3;
figure(3);
subplot(2,1,1);
plot(x, f);
title("Aufgabe 7 (b) - Figure 1");

clear f x;
x = -2:0.01:4;
f = -3.*x.^4 + 10.*x.^2 - 3;
subplot(2,1,2);
plot(x, f);
title("Aufgabe 7 (b) - Figure 2");

disp("Aufgabe 8 (a)");
figure(5);
subplot(2,1,1);
seats = [54, 46, 30, 28, 15, 12, 9, 2, 2, 1, 1];
txt = {
  'SVP';
  'SP';
  'FDP';
  'CVP';
  'GPS';
  'glp';
  'BDP';
  'EVP';
  'Lega';
  'CPS OW';
  'MCG';
};
pie(seats, txt);
title("Aufgabe 8 (a)");

disp("Aufgabe 8 (b)");
subplot(2,1,2);
seats = [35, 25, 18, 16, 12, 11, 10, 8, 8, 7, 7, 6, 6, 5, 5, 4, 4, 3, 2, 2, 1, 1, 1, 1, 1, 1];
txt = {
  'Zürich';
  'Bern';
  'Waadt';
  'Aargau';
  'St. Gallen';
  'Genf';
  'Luzern';
  'Tessin';
  'Wallis';
  'Basel-Landschaft';
  'Freiburg';
  'Solothurn';
  'Thurgau';
  'Basel-Stadt';
  'Graubünden';
  'Neuenburg';
  'Schwyz';
  'Zug';
  'Schaffhausen';
  'Jura';
  'Uri';
  'Obwalden';
  'Nidwalden';
  'Glarus';
  'Appenzell AR';
  'Appenzell IR';
};
pie(seats, txt);
title("Aufgabe 8 (b)");
