% Serie M4, MS (Marius Schär), SK (Severin Kaderli)
disp("Aufgabe 6");

frequency = 10000;
sounds = [
    450, 1
    350, 1
    450, 1
    350, 1
    500, 1
    300, 1
    500, 1
    300, 1
    450, 1
    350, 1
    450, 1
    350, 4
];
audio = createMelody(sounds, frequency);
audiowrite('M4_MS_SK_Glockenspiel.wav', audio, frequency);
sound(audio, frequency);

%% Glockenspiel erstellen
function audio = createMelody(sounds, frequency)
    audio = [];
    for i = 1:length(sounds)
        [Aenv, Ienv] = bellenv(1, 3, 2, sounds(1, 2), frequency);
        bell = synth(sounds(i, 1) - 100, sounds(i, 1), Aenv, Ienv, sounds(1, 2), frequency);        
        audio = cat(2, audio, bell);
    end
end

%% Generiert die Umhüllenden eines Glockentons.
function [Aenv, Ienv] = bellenv(A0, I0, tau, dur, fs)
    t = 0:1 / fs:dur - 1 / fs;
    Aenv = A0 * exp(- t / tau);
    Ienv = I0 * exp(- t / tau);
end

%% Synthesize
function cc = synth(fc, fm, Aenv, Ienv, dur, fs)
    time = 0:1 / fs:dur - 1 / fs;
 
    if (length(time) < length(Ienv))
        for i = length(time) + 1:length(Ienv)
            time(i) = time(i - 1) + 1 / fs;
        end
    elseif (length(time) > length(Ienv))
        time = time(1:length(Ienv));
    end
 
    cc = Aenv .* cos(2 * pi * fc .* time + Ienv .* cos(2 * pi * fm .* time - (pi / 2)) - pi / 2);
end

