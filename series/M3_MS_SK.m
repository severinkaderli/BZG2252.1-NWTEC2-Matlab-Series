% Serie M3, MS (Marius Schär), SK (Severin Kaderli)
disp("Aufgabe 1 (e)");
x = 0:0.001:0.48;
f = 2 * sin(10 * pi * x + 1);
plot(x, f);
title("Aufgabe 1 (e)");

disp("Aufgabe 4 ");
clear;
n = 1:4000;
x = cos(2*pi * 440 * n / 8192);
%sound(x);
figure(1);
plot(x);
title("Abbildung 1 von scham17, kades2")
print -dpng abbildung1_scham17_kades2.png

disp("Aufgabe 5");
clear;
load train;
whos;
%sound(y);
figure(2);
plot(y);
n = 1501:1700;
plot(n, y(n), ".-");
title("Abbildung 2 von scham17, kades2")
print -dpng abbildung2_scham17_kades2.png

disp("Aufgabe 6");
clear;
t = [1:8192]/8192;
z = cos(2 * pi * 440 * t) + cos(2 * pi * 444 * t);
sound(z);
% Klingt wie Option B
figure(3);
plot(z);
n = 1501:2000;
plot(n, z(n), ".-");
title("Abbildung 3 von scham17, kades2")
print -dpng abbildung3_scham17_kades2.png


